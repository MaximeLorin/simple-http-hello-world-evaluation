FROM maven:3.8.1-openjdk-17-slim AS maven
EXPOSE 8080
WORKDIR /usr/src/app
COPY . /usr/src/app

RUN mvn package

# For Java 11,
FROM eclipse-temurin:17-jre-alpine

ARG JAR_FILE=simple-http-hello-world-1.0-SNAPSHOT.jar

WORKDIR /opt/app

# Copy the wordle-jar.jar from the maven stage to the /opt/app directory of the current stage.
COPY --from=maven /usr/src/app/target/${JAR_FILE} /opt/app/

ENTRYPOINT ["java","-jar","simple-http-hello-world-1.0-SNAPSHOT.jar"]