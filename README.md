# Simple HTTP Hello World

Ce projet démarre un serveur HTTP basique qui répond aux requêtes HTTP sur le endpoint **/hello**.

Le serveur écoute sur le port **8181**. 

## Build du projet

Avec maven : `mvn package`. 

Le jar généré est disponible dans /target. Un jar auto-exécutable avec les dépendances du projet est disponible (sous le nom `*-jar-with-dependencies.jar`).

## Exemple

```
# Request
GET /hello

# Response
200 

Hello, world! It is currently 20:20:02.020
``` 

##Creer l'image Docker

Pour construire l'image docker a partir du dockerfile présent à la racine du dossier utiliser cette commande:

```
docker build -t  evaldevops . 
```

##Lancer le container

Pour lancer le container qui ecoute sur le port 8080 de notre machine et 8081 du container il faut utiliser la 
commande suivante:

```
docker run -d -p 8080:8081 --name containereval evaldevops
```

Note: J'ai rajouté --name afin de nommer le container comme je le veux.
